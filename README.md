# Notable

## Install dependencies

Run `npm i`

## Serve the frontend

Run `ng serve`

## Start the backend

Run `node server.js`

## Access the app

Open localhost:4200
