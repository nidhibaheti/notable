const path = require('path');
global.appRoot = path.resolve(__dirname);
const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 3000

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

const doctors  = [{
  id: 1,
  firstName: 'Julius',
  lastName: 'Hibbert',
  email: 'hibbert@notablehealth.com',
}, {
  id: 2,
  firstName: 'Algernop',
  lastName: 'Krieger',
  email: 'krieger@notablehealth.com'
}, {
  id: 3,
  firstName: 'Nick',
  lastName: 'Rivera',
  email: 'rivera@notablehealth.com'
}]

const appointments = [{
  id: 1,
  doctor_id: 1,
  patient_name: 'Sterling Archer',
  time: new Date().setHours(5),
  type: 'New Patient',
}, {
  id: 2,
  doctor_id: 1,
  patient_name: 'Cyril Figis',
  time: new Date().setHours(6),
  type: 'Follow up',
}, {
    id: 3,
    doctor_id: 2,
    patient_name: 'Lana Kane',
    time: new Date().setHours(10),
    type: 'New Patient',
  }, {
    id: 4,
    doctor_id: 3,
    patient_name: 'Cassie Archer',
    time: new Date().setHours(11),
    type: 'Follow up',

}]



app.get('/api/doctors', (req, res) => {
  res.status(200).json({
    response: doctors
  });
})

app.get('/api/appointments/:doctorId', (req, res) => {
  const doctorId = parseInt(req.params.doctorId);
  const appts = appointments.filter(appointment => appointment.doctor_id === doctorId);
  res.status(200).json({
    response: appts
  })
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
