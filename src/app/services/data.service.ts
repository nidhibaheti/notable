import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { HttpErrorHandler, HandleError } from './http-error-handler.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable()
export class DataService {
  serverUrl = 'api/';  // URL to web api
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('DataService');
  }

  /** GET doctors from the server */
  getDoctors() {
    return this.http.get(`${this.serverUrl}doctors`)
      .pipe(
        catchError(this.handleError('getDoctors', []))
      );
  }

  getAppointments(doctorId: number) {
    return this.http.get(`${this.serverUrl}appointments/${doctorId}`)
      .pipe(
        catchError(this.handleError('getAppointments', []))
      );
  }
}
