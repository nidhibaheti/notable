import {Component, OnInit} from '@angular/core';
import {DataService} from "./services/data.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Notable App';
  selectedDoctor;
  doctors = [];
  appointments = [];
  displayedColumns: string[] = ['id', 'name', 'time', 'kind'];

  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.dataService.getDoctors().subscribe((data: any) => {
      this.doctors = data.response;
    }, error => {
      console.error('Error', error);
    });
  }

  doctorClicked(doctor) {
    this.selectedDoctor = doctor;
    this.dataService.getAppointments(doctor.id).subscribe((data: any) => {
      console.log(data);
      this.appointments = data.response;
      console.log(this.appointments)
    }, error => {
        console.error('Error', error);
    }
    )
  }
}
